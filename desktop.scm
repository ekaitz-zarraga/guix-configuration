;; This is an operating system configuration generated
;; by the graphical installer.
(use-modules (gnu)
             (srfi srfi-1)
             ((gnu packages linux) #:prefix gnu-linux:)
             (gnu packages admin)
             (gnu packages certs)
             (gnu packages ssh)
             (gnu packages vim)
             (gnu packages wm)
             (gnu packages wget)
             (gnu packages xdisorg)
             (gnu packages xorg)
             (gnu packages video)
             (tuxedo-keyboard)
             (nongnu packages linux)
             (nongnu system linux-initrd))

(use-service-modules desktop networking ssh xorg)

(operating-system
  (kernel linux)
  (kernel-loadable-modules `(,tuxedo-keyboard))
  (initrd microcode-initrd)
  (firmware (list linux-firmware))
  (locale "en_US.utf8")
  (timezone "Europe/Madrid")
  (keyboard-layout
    (keyboard-layout "es"))
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets (list "/boot/efi"))
      (keyboard-layout keyboard-layout)

      ; Doesn't work (YET)
      #;(menu-entries
        (cons (menu-entry
                (label "Ubuntu - Tuxedo OS")
                (device "primary")
                (linux "/vmlinuz")
                (initrd "/initrd"))
              '()))))

  (swap-devices
    (list (swap-space (target "/dev/sda3"))))
  (file-systems
    (cons* (file-system
             (mount-point "/boot/efi")
             (device (uuid "6BB8-3AFE" 'fat32))
             (type "vfat"))
           (file-system
             (mount-point "/")
             (device
               (uuid "eee511d3-b473-4bc0-ba63-bf3971341576"
                     'ext4))
             (type "ext4"))
           (file-system
             (mount-point "/home")
             (device
               (uuid "49ebc09e-6021-4730-a467-b8818c2128f1"
                     'ext4))
             (type "ext4"))
           %base-file-systems))
  (host-name "tuxedo")
  (hosts-file
    (plain-file "hosts"
                (string-append (local-host-aliases host-name)
                               "51.255.193.230 vps\n")))

  (users (cons* (user-account
                  (name "Ekaitz")
                  (comment "Ekaitz")
                  (group "users")
                  (home-directory "/home/Ekaitz")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video" "lp" "dialout" "kvm")))
                %base-user-accounts))
  (packages
    (cons*
      htop
      gnu-linux:light
      i3-wm
      neovim
      nss-certs
      tree
      wget
      tcpdump
      openssh
      intel-vaapi-driver
      xf86-input-wacom
      %base-packages))

  (services (cons* (udev-rules-service 'light gnu-linux:light)
                   (service gnome-desktop-service-type)
                   (bluetooth-service #:auto-enable? #t)
                   (set-xorg-configuration
                     (xorg-configuration
                       (keyboard-layout keyboard-layout)
                       (modules (append %default-xorg-modules
                                        (list xf86-input-wacom)))
                       (extra-config
                         '("\n"
                           "\n"
                           "Section \"InputClass\"\n"
                           "  Identifier \"libinput touchpad catchall\"\n"
                           "  Driver \"libinput\"\n"
                           "  MatchIsTouchpad \"on\"\n"
                           "  MatchDevicePath \"/dev/input/event*\"\n"
                           "  Option \"Tapping\" \"on\"\n"
                           "  Option \"TappingDrag\" \"on\"\n"
                           "  Option \"DisableWhileTyping\" \"on\"\n"
                           "EndSection\n"
                           "\n"
                           ; https://wiki.archlinux.org/title/Libinput#Via_xinput
                           "Section \"InputClass\"\n"
                           "  Identifier \"USB Mouse Pad USB Mouse Pad Mouse\"\n"
                           "  Driver \"libinput\"\n"
                           "  Option \"Middle Emulation\" \"on\"\n"
                           "  Option \"AccelSpeed\"  \"-0.7\""
                           "EndSection\n"
                           "\n"
                           "\n"
                           "Section \"Device\"\n"
                           "  Identifier \"modesetting\"\n"
                           "  Driver \"modesetting\"\n"
                           "  Option \"TearFree\" \"True\"\n"
                           "EndSection"
                           "\n"
                           ))))
                   ; Removes ModemManager because it interferes with USB
                   ; development. If using a 3/4G modem or something we have
                   ; to activate it.
                   (remove (lambda (x) (eqv? (service-type-name
                                               (service-kind x))
                                             'modem-manager))
                           %desktop-services))))
